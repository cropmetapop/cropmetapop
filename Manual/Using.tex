\section{Using \textsc{CropMetaPop}}
This section describes how to install and use \textsc{CropMetaPop}.

\subsection{Installation}
The source code of \textsc{CropMetaPop} is available on the website
\url{www.cropmetapop.org}. It requires Python3, and the following
Python3 librairies:
\begin{itemize}
\item \textbf{simuPOP} (version $\geq$ 1.1.10): \textsc{CropMetaPop}
  uses simuPOP\cite{peng_simupop:_2005} to simulate the evolution of
  populations. (\url{http://simupop.sourceforge.net})
\item \textbf{igraph}: \textsc{CropMetaPop} uses the python module
  igraph to create default networks. If the network is directly given,
  this module is not necessary. (\url{http://igraph.org/python/})
\item \textbf{numpy}
\end{itemize}

\noindent A standalone version of \textsc{CropMetaPop} also exist in
the website. An up-to-date version can also be found on SourceSup.

\subsubsection{For Linux and MacOS}
The two libraries required by \textsc{CropMetaPop} can be installed
using pip with the following commands in a terminal:
\begin{verbatim}
$ pip3 install simuPOP
$ pip3 install python-igraph
$ pip3 install numpy
$ pip3 install cropmetapop
\end{verbatim}

\subsubsection{With Anaconda (especially for Windows)}
You first need to install Anaconda
(\url{https://www.anaconda.com/}). We recommend to create a new
environment for \textsc{CropMetaPop} using in the Anaconda prompt:
\begin{verbatim}
$ conda create -n cropmetapop
\end{verbatim}

\noindent You can then activate and update your environment:
\begin{verbatim}
$ conda activate cropmetapop
$ conda update conda
\end{verbatim}

\noindent Next, install the required libraries:
\begin{verbatim}
$ conda install numpy
$ conda install simupop
$ conda install python-igraph
$ conda install cropmetapop
\end{verbatim}

\subsection{Launching \textsc{CropMetaPop}}
\textsc{CropMetaPop} is a console program. A simulation is defined
using a settings file. The settings file is then passed as parameter
in the console when \textsc{CropMetaPop} is launched.
\begin{verbatim}
$ python3 CropMetaPop.py settings.txt
\end{verbatim}

\subsection{Input Files: settings of simulation}
The settings file is a text file with one parameter per line in a
`key:value' scheme.

\noindent For example: \texttt{generations:5} sets the parameter
generation to 5. The order of appearance of the parameters in the
settings file does not matter. If a parameter appears several times,
only the last one will be taken into account.

\subsubsection{Default value}
Most of the parameters have default values. The default value of a
parameter is taken into account when the parameter is not filled in
the settings file. The default values are given in this manual and are
specified by `(default:)'. Providing default values help in keeping
the settings file short and clear. All these default values are stored
in the `default\_setting.txt' file located in the source code.

\subsubsection{Parameter types}
Parameters may be of different types: integer, double, string, vector
or external file. The type of a parameter is specified in brackets '[
]'.

\paragraph{Integer}
Integer is a whole-number. Two forms are accepted: 1000 or 1e3.

\paragraph{Double}
Double is a floating-point number. Two forms are accepted: 0.001 or
1e-3.

\paragraph{String}
String is a text argument.

\paragraph{Vector}
Vector allows to pass several numbers (integer, double or String) as
parameter. Vector is enclosed between curly brackets '\{ \}', and
values are separated by comma ','.
\begin{verbatim}
nb_pop:5
init_size:{100,200,300,400,500}
\end{verbatim}

\noindent Usually vector are defined in whole. For example, the size
of vector for \textit{init\_size} is 5 as the number of population
(\textit{nb\_pop}). If the vector is a repetition of a pattern, it is
also possible to define only the pattern. In this case, the pattern is
automatically repeated as needed.
\begin{verbatim}
nb_pop:5
init_size:{200,100}
\end{verbatim}

In this example, there is 5 populations, however the initial size is
only specified for 2 populations. As 2 is lower than 5 a warning
message will be returned and the populations will automatically have
the following initial size: \texttt{init\_size:\{200, 100, 200, 100,
  200\}}. If all the values of a vector are identical, only one number
is needed. In the following example, the two specifications of the
initial size are equivalent:
\begin{verbatim}
nb_pop:5
init_size:{100,100,100,100,100}
init_size:100
\end{verbatim}


\paragraph{External file}
In general, arguments are written directly after the parameter name on
a single line. However, some parameters have to be given in external
text file. The format file is csv (comma-separated values). The
parameter value for external file is the name of the file. If the file
is in the current folder, the name is sufficient but otherwise the
path to the file is needed. To simplify, it's possible to use the
character '*' before the name of file if it is located in the same
folder as the settings file.
\begin{verbatim}
col_network:settings/matrixCol.csv
col_network:*matrixCol.csv
\end{verbatim}

\paragraph{Comments}
It is possible to write comments in the settings file. Comments have
to start with the `\#' character. For multi-line comments, this
character has to be used at the beginning of each line.

\subsection{Output Files}
\textsc{CropMetaPop} can produce up to 6 output files:
\begin{itemize}
\item 2 permanent files which are created at each simulation:
  \begin{itemize}
  \item a file giving, for each replicate and each population, the
    number of each mono-locus genotype, at each marker per generation
  \item a file giving the time of simulation, the warning messages and
    a summary of settings.
  \end{itemize}
\item 4 optional files:
  \begin{itemize}
  \item a file giving, for each replicate and each population, the
    number of each multi-locus genotype, per generation
  \item a file giving, for each replicate and each population, the
    number of each multi-locus haplotype, per generation
  \item a file giving the list of the number of individuals who moved
    from one population to another through migration per generation
  \item a file giving the list of the number of individuals who moved
    from one population to another through colonization per generation
  \end{itemize}
\end{itemize}
A copy of the settings file and other external files will be created
with outputs.

\subsection{Minimal settings file}
This section describes the parameters needed for a minimal settings
file and describes the simulated model.

\noindent In \textsc{CropMetaPop}, one parameter is needed in every
settings file:
\begin{verbatim}
carr_capacity:1000
\end{verbatim}

\noindent This minimal settings file allows to perform a simulation with:
\begin{itemize}
\item only one population consisting of 1000 hermaphrodites individuals.
\item the population evolves under random mating
\item during only one generation while keeping the population size
  constant and equal to the carrying capacity.
\item the default population has one marker with 2 alleles,
  initialized at 0.5.
\end{itemize}
