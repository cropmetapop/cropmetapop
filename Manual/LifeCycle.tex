\section{Life Cycle}

\textsc{CropMetaPop} relies on SimuPop, a discrete generation-based
simulator. This means that a single individual undergoes only once
during the life cycle without overlapping generations. Depending on
the parameterization of the settings file some processes can be
skipped. The order of the different processes of the life cycle is
fixed. A simulation starts with `Breeding', i.e.\@ only adults are
present at the initialization of the simulation. A new life cycle
starts again at the end of each generation up to the last generation,
i.e.\@ before the last generation a new life cycle starts with
”Breeding” and comes after `Output':
\begin{itemize}
\item \textbf{Breeding}: adult plants produce new seeds for the
  starting generation. Selection and mutation are taken into account
  at this stage. After reproduction, the adult plants are removed and
  the new seeds constitute the new generation.
\item \textbf{Extinction}: Due to stochastic events populations may
  extinct.
\item \textbf{Seed circulation}: A seed lot sample can be tranferred
  from one or several populations to another one.
\item \textbf{Regulation}: After these three previous steps, the
  number of seeds may be too large compared to the carrying
  capacity. This regulation stage allows to control the population
  sizes. The remaining seeds become adult plants that will be used to
  create the next generation.
\item \textbf{Output}: Different options are available to customize
  the output files after the simulations.
\end{itemize}

\subsection{Breeding}
This stage performs mating and breeding to produce the new offspring
generation. After that step, the adult plants are removed and the
population is composed of the offsprings. CropMetapop allows the
evolution of population size as the number of offsprings is the
produce between the parameter \textit{fecundity} and the number of
adults (\textit{number\_adult})
($number\_offspring = number\_adult \times fecundity$).

\paragraph[fecundity]{fecundity [Double] (Default:1)\label{par:fecundity}}
This parameter sets the average number of offspring produced per
individual. Default value for \textit{fecundity} is 1 corresponding to
a constant population size.
\begin{verbatim}
fecundity:1
\end{verbatim}

\subsubsection{Selection}
In this version of \textsc{CropMetaPop}, local selection is modeled as
a combination of soft selection and selection at the population
level. Therefore, selection influences the breeding at two levels. At
the population level, the effective number seeds produced in a
population is adjusted with the mean fitness of the adults of this
population ($number\_seed = number\_offspring \times
mean(fitness)$). At the individual level, the parents of each
offspring are drawn among all available parents within the population
depending on their individual fitness, i.e.\@ the higher the
individual fitness, the higher the probability to be chosen.

\textsc{CropMetaPop} simulates local selection in different
populations using two possibilities: either the same genotype may have
different fitness values depending on the population (in this case,
use the parameter \textit{fitness}) or each population has its own
optimum fitness (in this case, use the parameter
\textit{fitness\_equal}).

There are two types of markers:
\begin{itemize}
\item neutral markers do not influence the individual fitness
\item gene markers influence the individual fitness. Defining several
  gene markers allows to model a quantitative trait. The allelic
  effects at each locus must be set explicitly in the parameters
  \textit{fitness} or \textit{fitness\_equal}
\end{itemize}

\paragraph[fitness\_equal]{fitness\_equal [External file]}
This parameter allows to set a fitness value for each genotype at each
selective marker. Each line of this file is built as \textit{marker,
  genotype, value}. Marker in this file is automatically considered in
gene region. Default individual fitness value is equal to the optimum
of the population.
\begin{itemize}
\item \textit{marker [Integer]}: marker's identifier
\item \textit{genotype [String]}: The genotype must be written as
  \textit{Allele/Allele}.
\item \textit{value [Double]}: individual fitness value for the
  considered genotype at the considered marker. Individual fitness
  ranges from 0 to 1, by representing the relative reproductive
  success of an individual carrying this genotype for the considered
  marker.
\end{itemize}

\begin{verbatim}
nb_pop:3
fitness_equal:*fit.csv
\end{verbatim}

With the \texttt{fit.csv} file being of the following shape:
\begin{verbatim}
0, 1/1, 0.5
0, 1/0, 0.2
0, 0/0, 0.2
1, 1/1, 0.1
1, 1/0, 0.8
1, 0/0, 0.3
\end{verbatim}

From this file, the absolute fitness (fit\_abs) of one individual is
calculated as the mean of fitness values of its genotype at each
marker in gene region. In the above example, the markers 0 and 1 are
gene markers. An individual with a genotype 1/1 for the marker 0 and
the genotype 1/0 for the marker 1 has $fit\_abs = (0.5+0.8)/2 = 0.65$

\paragraph[fitness]{fitness [External file]}
This parameter allows to set the fitness values for each genotype at
each gene marker in each population. Each line of this file is built
as \textit{population, marker, genotype, value}. Marker in this file
is automatically considered as gene marker. Default fitness value is
fixed to the optimum of the population.

\begin{itemize}
\item \textit{population [Integer]}: number of number
\item \textit{marker [Integer]}: marker's ID
\item \textit{genotype [String]}: genotype must be written as
  \textit{numberOfAllele1/numberOfAllele2}.
\item \textit{value [Double]}: selective value between 0 and 1 for the
  genotype of the marker in this population. It describes the
  reproductive success of an individual with this genotype at this
  marker.
\end{itemize}

\begin{verbatim}
fitness:*fit.csv
\end{verbatim}

\begin{verbatim}
0, 0, 1/1, 0.5
1, 1, 1/0, 0.8
2, 0, 0/0, 0.9
2, 1, 0/1, 0.85
\end{verbatim}

\paragraph[optimum]{optimum [Double/vector] (Default:1)}

This parameter specifies the optimum of fitness for each
population. If the argument is a single value, the optimum is the same
for all the populations. By passing a vector of optima, it is possible
to assign an optimum value of fitness for each population.

\begin{verbatim}
nb_pop:2
optimum:0.8
\end{verbatim}

\begin{verbatim}
nb_pop:2
optimum:{0.2,0.8}
\end{verbatim}

In the first example, all the populations have an optimum of 0.8. In
the second example, population 0 has an optimum of 0.2 whereas
population 1 has an optimum of 0.8.

Finally, the fitness of one individual is given by:
\[fitness = e^{-\frac{1}{2} \times {(fit\_{abs} - optimumPopulation)}^2
  }\]

The closest to the optimum of the population is the absolute
fitness of an individual, the highest is the final fitness.

\subsubsection{Mating System}

In our model, individuals are hermaphrodite and individuals have two
possibilities for mating:
\begin{itemize}
\item \textit{Selfing}: one parent from a given population is randomly
  assigned to self fertilize and create a new individual
\item \textit{Random mating}: two parents from the same population are
  randomly assigned to create a new individual.
\end{itemize}
The proportion of selfing in the simulation is determined by the
parameter \textit{percentSelf}

\paragraph[percentSelf]{percentSelf [Double] (Default:0)}
This parameter allows to determine the proportion of offspring which
will be created by selfing. It must be between 0 and 1. When this
parameter is equal to 1, all the offsprings are created by selfing
whereas when it is equal to 0, all the offsprings are created by
random mating.

\begin{verbatim}
percentSelf:0.8
\end{verbatim}

In this case, 80\% of individuals are created by selfing and 20\% by
random mating.

\subsubsection{Mutation}
The mutation model used in \textsc{CropMetaPop} is a K-allele
model. In this model, each allele has the same probability to mutate
into another allele. The rate of mutation is defined by the parameter
\textit{mut\_rate}.

\paragraph[mut\_rate]{mut\_rate [Double/vector] (Default:0)}

This parameter specifies the mutation rate by locus and generation. If
the argument is a single value the mutation rate for all loci is the
same. By passing a matrix of mutation rates it is possible to set the
mutation rate for each single locus individually. By default no
mutations occur. The mutation rate ranges from 0 to 1.

\begin{verbatim}
nb_marker:2
mut_rate:0.01

nb_marker:2
mut_rate:{0.01,0.05}
\end{verbatim}
In the first example, the mutation rate for both markers is
0.01. However, in the second example, the marker 0 has a mutation rate
equal to 0.01 and equal to 0.05 for the marker 1.

\subsection{Extinction}
This event allows to randomly make population extinct. When a
population goes extinct, all individuals of the population are removed
and the patch becomes empty.
\paragraph[ext\_rate]{ext\_rate [Double/vector] (default: 0)}

The extinction rate corresponds to the probability of a population to
be extinct at each generation. It is specified by the parameter
\textit{ext\_rate}. This event is skipped when the extinction rate is
equal to zero. When this argument is a single value, all the
populations have the same extinction rate. It is possible to set the
extinction probability for each single population by passing a
vector. The extinction rate ranges from 0 to 1.

\begin{verbatim}
nb_pop:2
ext_rate:0.2

nb_pop:2
ext_rate:{0.2,0.3}
\end{verbatim}
In the first example, the extinction rate for both populations is
0.2. Whereas in the second example, the extinction rate is 0.2 for
population 0 and 0.3 for population 1.

\subsection{Seed Circulation}
Seed circulation is a two step process: the first one is composed of
colonization and/or migration models, and the second one by a seed
transfer model.

The colonization and migration models define the rules of seed
circulation and the seed transfer model defines the quantity of seed
in circulation.

Colonization occurs only to fill an empty patch while migration can
occur to fill an empty or not empty patch. Networks of seed
circulation are similarly defined for colonization and migration. Only
the name of the parameters are different (\textit{col\_param} and
\textit{migr\_param} respectively).

\subsubsection{Colonization model}
Colonization accounts for cases when a farmer has lost his population
due to an extinction event for instance and wants to obtain a new seed
lot to replace it. It is then possible for him to obtain seeds from
his social network through a direct neighbor who still grows the
population.

\paragraph{Required parameters} Along with the colonization network
that needs to be defined (\textit{cf}
\textsc{Section}~\ref{par:col_network}), you might want to make sure
the \texttt{fecundity} parameter (\textit{cf}
\textsc{Section}~\ref{par:fecundity}) default value fits your need. If
you are using colonization\_with\_excess, \texttt{fecundity} should be
set above 1 for colonisation events to happen.

\paragraph[col\_network]{col\_network [0,1,2,3,4,5,6 or external file] (Default 0)\label{par:col_network}}

This parameter allows to choose the network topology of seed
circulation for colonization, i.e the social network linking farmers
who possibly can provide seed lots. A network topology is defined by a
set of nodes (populations) and a set of edges (social links allowing
seed circulation between two populations). Two ways are available to
define the network topology: i.\@ when the parameter ranges from 1 to
6, different network models can be selected; ii.\@ when it is an
external file, it is possible to directly provide a matrix of
connection including colonization rates or an adjacency matrix.

\begin{enumerate}
\setcounter{enumi}{-1}
\item \textbf{No colonization}: populations are not connected with
  each other (empty network).
\item \textbf{Stepping stone 1D model}: each population is connected
  to 2 neighbors (chain).
\item \textbf{Stepping stone 2D model}: each population is connected
  to 4 neighbors (grid). With this colonization network, \textbf{the
    number of populations needs to have an integer square root}.
\item \textbf{Island model}: all the populations are connected with each other (complete network).
\item \textbf{Erdős-Rényi model}: a random graph model where each pair
  of populations has the same probability to be connected (Erdős \&
  Rényi, 1959).
\item \textbf{Community model}:a random graph model based on the
  stochastic block model defined by at least two clusters and two
  probability of connection (within and between cluster). The
  within-cluster probability needs to be higher than the
  between-cluster probability to obtain a community network topopology
  (Snijders \& Nowicki, 1997).
\item \textbf{Barabási-Albert model}:it is a random graph model based
  on preferential attachment algorithm (Albert \& Barabási, 2002). It
  is a discrete time step model and in each time step a single vertex
  is added. We start with a single vertex and no edges in the first
  time step. Then we add one vertex in each time step and the new
  vertex initiates some edges to old vertices. The probability that an
  old vertex is chosen is given by:$P[i] ~ k[i]^{\alpha}$ where k[i]
  is the in-degree of vertex i in the current time step (more
  precisely the number of adjacent edges of i which were not initiated
  by i itself) and $\alpha$ is a parameter given by the power
  arguments.
\end{enumerate}

\begin{tabular}{|c|p{0.6\linewidth}|}
  \hline
  Model & Additional Parameters\\
  \hline
  0: No colonization & \\
  \hline
  1: Stepping stone 1D model & \textit{col\_rate}\\
  \hline
  2: Stepping stone 2D model & \textit{col\_rate}\\
  \hline
  3: Island model & \textit{col\_rate}, \textit{col\_directed}\\
  \hline
  4: Erdős-Rényi model & \textit{col\_rate}, \textit{col\_directed}, \textit{col\_nb\_edge}\\
  \hline
  5: Community model & \textit{col\_rate}, \textit{col\_directed}, \textit{col\_nb\_cluster}, \textit{col\_prob\_intra}, \textit{col\_prob\_inter}\\
  \hline
  6: Barabási-Albert model & \textit{col\_rate}, \textit{col\_directed}, \textit{col\_power}\\
  \hline
\end{tabular}

\paragraph[col\_rate]{col\_rate [Double] (Default:0)}
This parameter defines the colonization rate between all pairs of
populations of the metapopulation. The value of this parameter ranges
from 0 to 1. The same colonization rate is applied to the network by
using this parameter. It is also possible to choose different
colonization rates by directly loading a matrix of connection
specifying each colonization rate using an external file (see
below). If this parameter is not filled, then populations are
considered as independent, corresponding to a situation without
colonization (equivalent to the option \texttt{col\_network=0}).

\paragraph[col\_directed]{col\_directed [0,1] (Default:0)}
This parameter defines if the created network will directed.
\begin{description}
\item [\textbf{0}:] The network won't be directed
\item [\textbf{1}:] The network will be directed
\end{description}

\paragraph[col\_nb\_edge]{col\_nb\_edge [Integer] (Default:0)}
This parameter sets the number of edges present in a network defined
through a Erdős-Rényi model. If this parameter is not filled for an
Erdős-Rényi model, then the number of edge is set to 0 and populations
are considered as independent, corresponding to a situation without
colonization (equivalent to the option \texttt{col\_network=0}).

\paragraph[col\_nb\_cluster]{col\_nb\_cluster [Integer] (default:1)}
This parameter allows to set the number of clusters present a network
topology obtained through the Community model.

\paragraph[col\_prob\_intra]{col\_prob\_intra [Double] (Default:1)}
This parameter allows to set the within-cluster probability of
connection in the Community model, i.e.\@ the probability for two
populations from the same cluster to be connected together. The value
of this parameter ranges from 0 to 1.

\paragraph[col\_prob\_inter]{col\_prob\_inter [Double] (Defaut:1)}
This parameter allows to set the between-cluster probability of
connection in the Community model, i.e.\@ the probability for two
populations from two different clusters to be connected together. The
value of this parameter ranges from 0 to 1.

\paragraph[col\_power]{col\_power [Double] (Default:1)}
This parameter allows to set the power for the Barab\'asi-Albert.

It is also possible to give a self-defined connection matrix of
desired network with an external file. Two ways are possible to define
such colonization model: i.\@ the external file is a matrix filled
with 0 and 1, corresponding to an adjacency matrix and the
\textit{col\_rate} option is used to defined the general colonization
rate of the network; ii.\@ the external file is filled with values
ranging from 0 to 1, corresponding to local colonization rate.

\begin{verbatim}
col_network:*matrix.csv
col_rate:0.2

0, 0, 1
1, 0, 1
0, 1, 0
\end{verbatim}

or

\begin{verbatim}
col_network:*matrix.csv

0, 0, 0.2
0.2, 0, 0.2
0, 0.2, 0
\end{verbatim}

These 2 examples give the same results corresponding to the two above
mentioned possibilities respectively.
\subsubsection{Migration model}
Migration accounts for cases when a farmer introduces a new source of
seed into his population. It is then possible for him to obtain
additional seeds from his social network through a direct neighbor who
grows the population.

\paragraph{Required parameters:} Along with the migration network that
needs to be defined (\textit{cf}
\textsc{Section}~\ref{par:migr_network}), you might want to make sure
the \texttt{fecundity} parameter (\textit{cf}
\textsc{Section}~\ref{par:fecundity}) default value fits your need. If
you are using migration\_with\_excess, \texttt{fecundity} should be
set above 1 for colonisation events to happen.

\noindent The \texttt{migr\_replace} parameter (\textit{cf} \textsc{Section}~\ref{par:migrReplace}) default value must also
be replaced in your configuration file in order for migration events
to happen.

\paragraph[migr\_network]{migr\_network [0,1,2,3,4,5,6 or external file] (Default
  0)\label{par:migr_network}}

This parameter allows to choose the network topology of seed
circulation for migration, i.e the social network linking farmers who
possibly can provide seed lots. A network topology is defined by a set
of nodes (populations) and a set of edges (social links allowing seed
circulation between two populations). Two ways are available to define
the network topology: i.\@ when the parameter ranges from 1 to 6,
different network models can be selected; ii.\@ when it is an external
file, it is possible to directly provide a matrix of connection
including migration rates or an adjacency matrix.

\begin{enumerate}
\setcounter{enumi}{-1}
\item \textbf{No migration}: populations are not connected with each
  other (empty network).
\item \textbf{Stepping stone 1D model}: each population is connected
  to 2 neighbors (chain).
\item \textbf{Stepping stone 2D model}: each population is connected
  to 4 neighbors (grid). With this migration network, \textbf{the
    number of populations needs to have an integer square root}.
\item \textbf{Island model}: all the populations are connected with
  each other (complete network).
\item \textbf{Erdős-Rényi model}: a random graph model where each pair
  of populations has the same probability to be
  connected\cite{erdos_random_1959}.
\item \textbf{Community model}:a random graph model based on the
  stochastic block model defined by at least two clusters and two
  probability of connection (within and between cluster). The
  within-cluster probability needs to be higher than the
  between-cluster probability to obtain a community network
  topopology\cite{snijders_estimation_1997}.
\item \textbf{Barabási-Albert model}:it is a random graph model based
  on preferential attachment algorithm\cite{albert_statistical_2002,
    barabasi_emergence_1999}. It is a discrete time step model and in
  each time step a single vertex is added. We start with a single
  vertex and no edges in the first time step. Then we add one vertex
  in each time step and the new vertex initiates some edges to old
  vertices. The probability that an old vertex is chosen is given
  by:$P[i] ~ k[i]^{\alpha}$ where k[i] is the in-degree of vertex i in
  the current time step (more precisely the number of adjacent edges
  of i which were not initiated by i itself) and $\alpha$ is a
  parameter given by the power arguments.
\end{enumerate}

\begin{tabular}{|c|p{0.6\linewidth}|}
  \hline
  Model & Additional Parameters\\
  \hline
  0: No migration & \\
  \hline
  1: Stepping stone 1D model & \textit{migr\_rate}\\
  \hline
  2: Stepping stone 2D model & \textit{migr\_rate}\\
  \hline
  3: Island model & \textit{migr\_rate}, \textit{migr\_directed}\\
  \hline
  4: Erdős-Rényi model & \textit{migr\_rate}, \textit{migr\_directed}, \textit{migr\_nb\_edge}\\
  \hline
  5: Community model & \textit{migr\_rate}, \textit{migr\_directed}, \textit{migr\_nb\_cluster}, \textit{migr\_prob\_intra}, \textit{migr\_prob\_inter}\\
  \hline
  6: Barabási-Albert model & \textit{migr\_rate}, \textit{migr\_directed}, \textit{migr\_power}\\
  \hline
\end{tabular}

\paragraph[migr\_rate]{migr\_rate [Double] (Default:0)}
This parameter defines the migration rate between all pairs of
populations of the metapopulation. The value of this parameter ranges
from 0 to 1. The same migration rate is applied to the network by
using this parameter. It is also possible to choose different
migration rates by directly loading a matrix of connection specifying
each migration rate using an external file (see below). If this
parameter is not filled, then populations are considered as
independent, corresponding to a situation without migration
(equivalent to the option \texttt{migr\_network=0}).

\paragraph[migr\_directed]{migr\_directed [0,1] (Default:0)}
This parameter defines if the created network will directed.
\begin{description}
\item [\textbf{0}:] The network won't be directed
\item [\textbf{1}:] The network will be directed
\end{description}

\paragraph[migr\_nb\_edge]{migr\_nb\_edge [Integer] (Default:0)}
This parameter sets the number of edges present in a network defined
through a Erdős-Rényi model. If this parameter is not filled for an
Erdős-Rényi model, then the number of edge is set to 0 and populations
are considered as independent, corresponding to a situation without
migration (equivalent to the option \texttt{migr\_network=0}).

\paragraph[migr\_nb\_cluster]{migr\_nb\_cluster [Integer] (default:1)}
This parameter allows to set the number of clusters present a network
topology obtained through the Community model.

\paragraph[migr\_prob\_intra]{migr\_prob\_intra [Double] (Default:1)}
This parameter allows to set the within-cluster probability of
connection in the Community model, i.e. the probability for two
populations from the same cluster to be connected together. The value
of this parameter ranges from 0 to 1.

\paragraph[migr\_prob\_inter]{migr\_prob\_inter [Double] (Defaut:1)}
This parameter allows to set the between-cluster probability of
connection in the Community model, i.e.\@ the probability for two
populations from two different clusters to be connected together. The
value of this parameter ranges from 0 to 1.

\paragraph[migr\_power]{migr\_power [Double] (Default:1)}
This parameter allows to set the power for the Barab\'asi-Albert.

It is also possible to give a self-defined connection matrix of
desired network with an external file. Two ways are possible to define
such migration model: i.\@ the external file is a matrix filled with 0
and 1, corresponding to an adjacency matrix and the
\textit{migr\_rate} option is used to defined the general migration
rate of the network; ii.\@ the external file is filled with values
ranging from 0 to 1, corresponding to local migration rate.

\begin{verbatim}
migr_network:*matrix.csv
migr_rate:0.2

0, 0, 1
1, 0, 1
0, 1, 0
\end{verbatim}

or

\begin{verbatim}
migr_network:*matrix.csv

0, 0, 0.2
0.2, 0, 0.2
0, 0.2, 0
\end{verbatim}

These 2 examples give the same results corresponding to the two above
mentioned possibilities respectively.

\subsubsection{Seed transfert model}
The seed transfer model allows to set the parameters necessary to
define the rules to quantify the seed in circulation during seed
circulation events.

\paragraph[col\_transfer\_model \textit{\&}
migr\_transfer\_model]{col\_transfer\_model \textit{\&}
  migr\_transfer\_model [String] (Default:
  `excess')\label{par:transferModel}}
These parameters determine the seed transfer model used to calculate
how many seeds will be transferred among populations during the
colonization event and the migration event respectively.
\begin{itemize}
\item \textbf{excess}: this parameter value corresponds to the
  situation when a farmer provides seed to another farmer only if his
  population produces more seeds than its carrying capacity.\\
  Furthermore, the number of outgoing seeds is adjusted with the
  carrying capacity of the recipient population.\\
  The number of seed transferred is thus calculated as the minimum
  between the donor capacity to provide seed and the recipient
  capacity to receive seeds. If a farmer provides seeds to several
  farmers, the global capacity to provide seeds of the donor is
  equally distributed among the recipients. Likewise, if a population
  receives seeds from several populations, then the total amount of
  seeds received is equally distributed among the seed sources.
\item \textbf{friendly}: this parameter value corresponds to the
  situation when a farmer provides seed to another farmer even if his
  population don’t produces enough seed for sowing all the field.
\end{itemize}

\paragraph[col\_from\_one \textit{\&} migr\_from\_one]{col\_from\_one
  \textit{\&} migr\_from\_one [0,1] (Default:0)}
These parameters allow to reduce the send of seed in colonisation or
migration from only one population.
\begin{description}
\item [\textbf{0}]: A population can received seeds from several
  populations in one colonisation or migration event.
\item [\textbf{1}]: A population can received seed from only one
  population in one colonisation or migration event.
\end{description}

\paragraph[migr\_carrying]{migr\_carrying [0,1] (Default:0)}
This parameter allows to control if a population can receive migrant
seeds when it isn't at its carrying capacity.
\begin{description}
\item [\textbf{0}]: The recipient population has to be at its carrying
  capacity to receive seeds by migration.
\item [\textbf{1}]: The recipient population does not need to be at
  its carrying capacity to receive seeds by migration. In this case,
  the migrant seeds complete the local seed without exceeding the
  carrying capacity.
\end{description}

\paragraph[migr\_replace]{migr\_replace [Double/vector] (Default:0)\label{par:migrReplace}}
This parameter allows to set the replacement rate for each
population. This corresponds to the percentage of local seeds that
will be replaced by the migrant seeds. If \textit{migr\_replace} is a
single value, then it is the same replacement rate for all the
populations. If it is a vector, then a replacement rate is defined for
each population. The value must be between 0 and 1.

\paragraph[col\_keepRate \textit{\&} migr\_keepRate]{col\_keepRate
  [Double/vector] (Default:0.5) \& migr\_keepRate [Double/vector]
  (Default:0.5)}
These parameters allows to set the minimum proportion that the
population of origin of the seeds keeps instead of sharing with
requesting populations. This parameter can be used both for
colonisation and for migration.

They are only used when the transfer model (see
\textsc{Section}~\ref{par:transferModel}) is set to friendly. When it
is, make sure to redefine it as you need.

\subsection{Regulation}
This event allows to limit the temporary population size obtained
after the previous steps to its \textit{carrying capacity}.

After this step, the seeds become adult individuals (plants), starting
a new life cycle by reproducing during the breeding process.

\subsection{Outputs}
At the end of each life cycle, i.e.\@ one generation, information
corresponding to the mono-locus genotypes of individuals are stored to
be written in the result file after the simulation. This information
is stored at every step determined by the parameter \textit{step}.

\paragraph[step]{step [Integer] (Default:1)}
This parameter corresponds to the frequency to store individual
information into output files.

\begin{verbatim}
step:5
\end{verbatim}

In this example, data is stored every 5 generations.

\paragraph[outputs]{outputs [String/vector]}
This parameter defines optional results files which will be created
after the simulation.

\begin{itemize}
\item \textbf{Genotype}: creates a file with the number of each unique
  multi-locus genotype per population, for all the requested
  generations and all the replicates. The time to write this file is
  proportional to the number of markers used in the simulation.
\item \textbf{Haplotype}: creates a file with the number of each
  haplotype per population for all the requested generations and all
  the replicates.
\item \textbf{Seed\_transfert}: creates two files summarizing the
  different seed transfer events and seed quantity at each generation
  for all the populations and replicate. One file corresponds to the
  colonization events and the second to the migration events.
\end{itemize}

The following exammples are presented for:
\begin{verbatim}
nb_pop:2
nb_allele:2
nb_marker:1
\end{verbatim}

\noindent Mono-locus genotype result example:\\
\begin{tabular}{|c|c|c|c|c|c|c|c}
  \hline
  Replicate & Population & Marker & Genotype & Gen 0 & Gen 1 & Gen 2 & \ldots\\
  \hline
  0& 0& 0& 0/0& 25&28&31& \ldots\\
  \hline
  0& 0& 0& 0/1& 49&20&19& \ldots\\
  \hline
  0& 1& 1& 1/1& 26&28&31& \ldots\\
  \hline
  \vdots &\vdots &\vdots &\vdots &\vdots &\vdots &\vdots &$\ddots$\\
\end{tabular}

~\\~\\
Multi-locus genotype result example:\\
\begin{tabular}{|c|c|c|c|c|c|c|c}
  \hline
  Replicate & Population & Marker 0 & Marker 1 & Gen 0 & Gen 1 & Gen 2 & \ldots\\
  \hline
  0& 0& 0/0& 0/0& 25&28&31& \ldots\\
  \hline
  0& 0& 0/0& 1/0& 22&20&19& \ldots\\
  \hline
  0& 1& 0/0& 0/0& 26&28&31& \ldots\\
  \hline
  \vdots &\vdots &\vdots &\vdots &\vdots &\vdots & \vdots & $\ddots$\\
\end{tabular}

~\\~\\
Haplotype result example:\\
\begin{tabular}{|c|c|c|c|c|c|c|c}
  \hline
  Replicate & Population & Marker 0 & Marker 1 & Gen 0 & Gen 1 & Gen 2 & \ldots\\
  \hline
  0& 0& 0& 0& 25&28&31& \ldots\\
  \hline
  0& 0& 0& 1& 22&20&19& \ldots\\
  \hline
  0& 1& 0& 0& 26&28&31& \ldots\\
  \hline
  \vdots &\vdots &\vdots &\vdots &\vdots &\vdots & \vdots & $\ddots$\\
\end{tabular}

~\\~\\
Colonization result example:\\
\begin{tabular}{|c|c|c|c|c|c|c}
  \hline
  Replicate & Source & Target & Gen 0 & Gen 1 & Gen 2 & \ldots\\
  \hline
  0& 0& 1& 25&0&31& \ldots\\
  \hline
  0& 0& 2& 22&0&0& \ldots\\
  \hline
  0& 1& 0& 0&28&31& \ldots \\
  \hline
  \vdots &\vdots &\vdots &\vdots &\vdots & \vdots & $\ddots$\\
\end{tabular}

~\\~\\
\texttt{outputs:Haplotype,Seed\_transfert}

\paragraph[separate\_replicate]{separate\_replicate [0,1] (Default:0)}
This parameter allows to separate the results by replicates.
\begin{description}
\item [\textbf{0}]: All results are in a same file
\item [\textbf{1}]: A different file is created for each replicate.
\end{description}
