\section{Meta-Population}
This section describes general parameters concerning the
metapopulation.

\paragraph[nb\_pop]{nb\_pop [Integer] (Default:1)}
This parameter allows to set the number of populations. The numbering
of populations begins to 0.

\paragraph[init\_size]{init\_size [Integer/vector]}: This parameter allows to set
different population sizes for the initialization generation. If the
argument is a single value, the initial population size is the same
for all populations. It is possible to set an initial size for each
population by providing a vector of length equal to the number of
populations.

\noindent If the initial population size (parameter \texttt{init\_size}) is not
specified, populations are initialized with carrying capacity values.

\paragraph[carr\_capacity]{carr\_capacity [Integer/vector]}: This parameter is the
only parameter required in the settings file. This parameter allows to
set the carrying capacities of the populations. The carrying capacity
of a population is the maximal number of individuals supported by the
patch (field) where the population grows. If the argument is a single
value, the carrying capacity is the same for all the populations. It
is possible to set the carrying capacity for each population by
providing a vector of length equal to the number of populations.
\begin{verbatim}
nb_pop:2
carr_capacity:100

nb_pop:2
carr_capacity:{100,200}
\end{verbatim}

\noindent In the first example, all populations have a carrying
capacity equal to 100 individuals. In the second example, the
population 0 has a carrying capacity equal to 100 individuals while
the population 1 has a carrying capacity equal to 200 individuals.

\paragraph[nb\_marker]{nb\_marker [Integer] (Default:1)}: This parameter allows to
set the number of markers per individual. The numbering of markers
begins to 0.

\paragraph[nb\_allele]{nb\_allele [Integer/vector] (Default:2)}: This parameter
allows to set the number of alleles per marker. The numbering of
alleles begins to 0.
\begin{verbatim}
nb_marker:3
nb_allele:2
\end{verbatim}

\noindent In the above example, there is 3 markers with 2 alleles for
each of them.

\begin{verbatim}
nb_marker:3
nb_allele:{2,3,4}
\end{verbatim}

\noindent In the above example, there is 3 markers. The first marker
has 2 alleles, the second has 3 alleles and the third has 4 alleles.


\noindent\textsc{CropMetaPop} recognizes 4 forms of input format for the
initialization of individual genotypes, corresponding to 4 parameters:
\texttt{init\_AlleleFrequency}, \texttt{init\_AlleleFrequency\_equal},
\texttt{init\_GenotypeFrequency} and
\texttt{init\_GenotypeFrequency\_equal}. Only one parameter has to be
used in the settings file. If none of them is used, the populations
are initialized with the same frequency for every allele of the same
marker. This frequency is equal to one over the number of alleles per
marker.

\paragraph[init\_AlleleFrequency]{init\_AlleleFrequency [External
  file]}
This parameter allows to set the initial allele frequencies of each
population. This frequency may be different depending to the
population. This parameter requires a text file using comma as
separator (type csv). Each line of this file is organized as follows:
\textit{population, marker, frequencyAllele\_0, frequencyAllele\_1,
  \ldots}, with:
\begin{itemize}
\item \textit{population [Integer]}: number of the population
\item \textit{marker [Integer]}: number of the marker
\item \textit{frequencyAllele\_i [Double]}: frequency ranging from 0
  to 1 for the allele \textit{i} of the marker.
\end{itemize}

\noindent Allele frequencies have to sum to 1 for every line. The
initial frequencies for all markers for all populations must be
given. If the frequency of one allele is not given, it is considered
equal to 0.

\begin{verbatim}
nb_pop:2
nb_marker:3
nb_allele:{2,3}
init_AlleleFrequency:*freq.csv
\end{verbatim}

The file \texttt{freq.csv} is the following:

\begin{verbatim}
0, 0, 0.5, 0.5
0, 1, 0.2, 0.3, 0.5
0, 2, 0.2, 0.8
1, 0, 0.5, 0.5
1, 1, 0.5, 0, 0.5
1, 2, 1
\end{verbatim}


It is possible to use the parameter
\textit{init\_AlleleFrequency\_equal} to set the same initial allele
frequency for all populations, as described next.

\paragraph[init\_AlleleFrequency\_equal]{init\_AlleleFrequency\_equal
  [External file]}
This parameter allows to set the same initial allele frequencies for
each population. This parameter requires a text file using comma as
separator (type csv). Each line of this file is organized as follows:
\textit{marker, frequencyAllele\_0, frequencyAllele\_1, \ldots}, with:

\begin{itemize}
\item \textit{marker [Integer]}: number of the marker
\item \textit{frequencyAllele\_i [Double]}: frequency range from 0 to
  1 for the allele \textit{i} of the marker.
\end{itemize}

\noindent Allele frequencies have to sum to 1 for every line. The
initial allele frequencies for all markers must be given. If the
frequency of one allele is not given, it is considered equal to 0.

\begin{verbatim}
nb_pop:2
nb_marker:3
nb_allele:{2,3}
init_AlleleFrequency_equal:*freq.csv
\end{verbatim}

\noindent The file \texttt{freq.csv } is the following:
\begin{verbatim}
0, 0.5, 0.5
1, 0.2, 0.3, 0.5
2, 1
\end{verbatim}

\paragraph[init\_GenotypeFrequency]{init\_GenotypeFrequency [External
  file]}

This parameter allows to set the initial multi-locus genotype
frequencies for each population. This frequency may be different
depending on the population. The composition in terms of multi-locus
genotypes can also be different depending on the population. This
parameter requires a text file using comma as separator (type
csv). Each line of this file is built as follows: \textit{population,
  genotypeMarker\_1, genotypeMarker\_2, \ldots, frequency}, with:

\begin{itemize}
\item \textit{population [Integer]}: number of the population
\item \textit{genotypeMarker\_i [String]}: The genotype of the marker
  \textit{i} must be writen as \textit{allele/allele}.
\item \textit{frequency [Double]}: frequency between 0 and 1 of the
  multi-locus genotype in the population.
\end{itemize}

\noindent In this file, all the markers have to be filled. All the multi-locus
genotypes per population have to be provided. Multi-locus frequencies
have to sum to 1 per population.

\begin{verbatim}
nb_pop:3
nb_marker:2
nb_allele:{2,3}
init_GenotypeFrequency:*freq.csv
\end{verbatim}

\noindent The file \texttt{freq.csv } is the following:
\begin{verbatim}
0, 1/1, 1/1, 0.5
0, 1/0, 0/0, 0.5
1, 0/0, 0/0, 0.3
1, 1/0, 1/2, 0.5
1, 0/0, 1/1, 0.2
2, 1/1, 0/0, 1
\end{verbatim}

\noindent It is possible to use the parameter
\textit{init\_GenotypeFrequency\_equal} if all the populations have
the same initial genotypes, as described next.

\paragraph[init\_GenotypeFrequency\_equal]{init\_GenotypeFrequency\_equal
  [External file]}
This parameter allows to set the multi-locus genotype frequencies per
population during initialization. This parameter requires a text file
using comma as separator (type csv).  Each line of this file is built
as follows: \textit{genotypeMarker\_0, genotypeMarker\_1, \ldots,
  frequency}, with:
\begin{itemize}
\item \textit{genotypeMarker\_i [String]}: The genotype of the marker
  i must be write as \textit{allele/allele}.
\item \textit{frequency [Double]}: frequency between 0 and 1 of the
  multi-locus genotype.
\end{itemize}

\noindent In this file, all the markers have to be filled. The
multi-locus genotypes frequencies has to sum to 1.

\begin{verbatim}
nb_pop:2
nb_marker:3
nb_allele:{2,3}
init_GenotypeFrequency_equal:*freq.csv
\end{verbatim}

The file \texttt{freq.csv} is of the following shape:
\begin{verbatim}
1/1, 1/1, 0.25
0/0, 0/2, 0.25
0/0, 1/1, 0.5
\end{verbatim}

\paragraph[geneticMap]{geneticMap [External file]}
This file allows to give the position of each marker on
chromosome. This parameter requires a text file using comma as
separator (type csv). Each line of this file is built as follows:
\textit{marker, chromosome, distance}, with:
\begin{itemize}
\item \textit{marker [Integer]}: number of marker
\item \textit{chromosome [Integer]}: number of chromosome
\item \textit{distance [Double]}: This distance between the extremity
  of chromosome and the marker, in morgan.
\end{itemize}
All the markers have to be filled. Moreover, markers have to be
ordered by distance.

\begin{verbatim}
geneticMap:*genetic.csv
\end{verbatim}

The file \texttt{genetic.csv} with the configuration file of the
previous example is of the following shape:
\begin{verbatim}
0, 1, 0.5
1, 2, 1
3, 2, 2.5
\end{verbatim}

\noindent The following formula is used to calculate the recombination
rate between pairs of markers (Haldane) thanks to the disctance
between two adjacent markers (D):
\[ recombination = \frac{1}{2}(1-e^{-2D})\]

\noindent If the parameter \textit{geneticMap} is not given, each marker is
considered as independent and the recombination rate is fixed to 0.5.
